﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using owstats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace owstats.Tests
{
    [TestClass()]
    public class GameTests
    {
        [TestMethod()]
        public void GameConstructed()
        {
            //arrange
            string[] heroesPlayed = { "Ana", "Mercy" };
            
            //act
            Game newGame = new Game(3500, 1, "Assault", "Anubis", heroesPlayed, DateTime.Now);

            //assert
            Assert.IsNotNull(newGame);
        }

        [TestMethod]
        public void getSR()
        {
            //arrange
            string[] heroesPlayed = { "Ana", "Mercy" };
            Game newGame = new Game(3500, 1, "Assault", "Anubis", heroesPlayed, DateTime.Now);

            //act
            var data = newGame.GetSR();

            //assert
            Assert.IsNotNull(data);
        }

        [TestMethod]
        public void getType()
        {
            //arrange
            string[] heroesPlayed = { "Ana", "Mercy" };
            Game newGame = new Game(3500, 1, "Assault", "Anubis", heroesPlayed, DateTime.Now);

            //act
            var data = newGame.GetMatchType();

            //assert
            Assert.IsNotNull(data);
        }

        [TestMethod]
        public void getMap()
        {
            //arrange
            string[] heroesPlayed = { "Ana", "Mercy" };
            Game newGame = new Game(3500, 1, "Assault", "Anubis", heroesPlayed, DateTime.Now);

            //act
            var data = newGame.GetMap();

            //assert
            Assert.IsNotNull(data);
        }

        [TestMethod]
        public void getHeroes()
        {
            //arrange
            string[] heroesPlayed = { "Ana", "Mercy" };
            Game newGame = new Game(3500, 1, "Assault", "Anubis", heroesPlayed, DateTime.Now);

            //act
            var data = newGame.GetHeroes();

            //assert
            Assert.IsNotNull(data);
        }

        [TestMethod]
        public void getTime()
        {
            //arrange
            string[] heroesPlayed = { "Ana", "Mercy" };
            Game newGame = new Game(3500, 1, "Assault", "Anubis", heroesPlayed, DateTime.Now);

            //act
            var data = newGame.GetTime();

            //assert
            Assert.IsNotNull(data);
        }
    }
}