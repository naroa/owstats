﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using owstats;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace owstats.Tests
{
    [TestClass()]
    public class Stat_trackerTests 
    {
        string savedDataLocation = "C:/Users/yonan/Documents/Visual Studio 2017/Projects/owstats/owstatsTests/bin/Debug/owstats.data";
        string seasonGamesLocation = "C:/Users/yonan/Documents/Visual Studio 2017/Projects/owstats/owstatsTests/bin/Debug/owstats.games";

        [TestMethod()]
        public void StatTrackTest() //as of right now, this test is designed to fail. if it fails, we're good. if it passes, we're not good. :^)
        {
            //arrange
            Stat_tracker addGameTest = new Stat_tracker();

            //act
            addGameTest.StatTrackTester();

            //assert
            Assert.Fail(); 
        }

        [TestMethod()]
        public void EmptyStorage()
        {
            //arrange
            Stat_tracker emptyStorageTest = new Stat_tracker();
            using (StreamWriter dataLocation = File.CreateText(savedDataLocation))
            {
                //gamecount, calibrated sr, currentsr, wins,losses,ties
                dataLocation.WriteLine("50");
                dataLocation.WriteLine("2759");
                dataLocation.WriteLine("3524");
                dataLocation.WriteLine("35");
                dataLocation.WriteLine("10");
                dataLocation.WriteLine("5");
            }
            var deleteData = new FileInfo(savedDataLocation);
            var gamesLocation = new FileInfo(seasonGamesLocation);

            //act
            var result = emptyStorageTest.LoadData(deleteData, gamesLocation);

            //assert
            deleteData.Delete();
            gamesLocation.Delete();
            Assert.IsTrue(result);
        }

        [TestMethod()]
        public void StorageExists()
        {
            //arrange
            Stat_tracker storageExistsTest = new Stat_tracker();
            using (FileStream dataLocation = File.Create(savedDataLocation))
            {
                
            }
            var deleteData = new FileInfo(savedDataLocation);
            var gamesLocation = new FileInfo(seasonGamesLocation);

            //act
            var result = storageExistsTest.LoadData(deleteData,gamesLocation);

            //assert
            deleteData.Delete();
            Assert.IsFalse(result);
        }

        [TestMethod()]
        public void StorageDoesNotExist()
        {
            //arrange
            Stat_tracker StorageDoesNotExistTest = new Stat_tracker();
            var deleteData = new FileInfo(savedDataLocation);
            var gamesLocation = new FileInfo(seasonGamesLocation);
            if (deleteData.Exists)
                deleteData.Delete();

            //act
            var result = StorageDoesNotExistTest.LoadData(deleteData,gamesLocation);

            //assert
            Assert.IsFalse(result);
        }

        [TestMethod()]
        public void GameSavesSuccessfully()
        {
            //arrange
            Stat_tracker SaveTheData = new Stat_tracker();

            //act
            SaveTheData.StatTrackTester();

            //assert
            bool existed;
            if (new FileInfo(seasonGamesLocation).Exists)
                existed = true;
            else
                existed = false;
            var gamesLocation = new FileInfo(seasonGamesLocation);
            gamesLocation.Delete();
            Assert.IsTrue(existed);
        }
    }
}