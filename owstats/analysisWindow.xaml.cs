﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.DataVisualization.Charting;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace owstats
{
    /// <summary>
    /// Interaction logic for analysisWindow.xaml
    /// </summary>
    /// 
    public partial class AnalysisWindow : Window
    {
        Stat_analyzer stats = new Stat_analyzer();
        string currentHero;
        string currentMap;
        string currentGroup;

        public AnalysisWindow()
        {
            InitializeComponent();
        }

        private void Populate_Details(string filter, Statistics details)
        {
            ExtendedData.Visibility = Visibility.Visible;
            Calculations.Content = "Wins: " + details.winCount + "\n" + "Losses: " + details.lossCount + "\n" + "Ties: " + details.tieCount + "\n" +
                "Winrate: " + Math.Round(details.winrateWithTies * 100, 2) + "%\n" + "Winrate(w/o ties): " + Math.Round(details.winrate * 100, 2) + "%";
            Populate_Chart(filter);
            ExtendedData.Header = filter;
            if( currentHero != null)
                currentHero = Reabbreviate(currentHero);
            if (currentMap != null)
                currentMap = Reabbreviate(currentMap);          //these two are the products of SHIT TIER CODING, but this should resolve issues either way.
        }

        private void Populate_Chart(string filter)
        {
            VisualizedData.Visibility = Visibility.Visible;
            LoadSROverTimeChart();
        }

        private void LoadSROverTimeChart()
        {
            //the idea here is that we want to show the winrate as a function of games. This means we can't do that kind of analysis on just end data, this needs to be populated as the end result is interpreted.
            var filteredGAEMS = stats.AccessFilteredGames();
            
            //new KeyValuePair<>


            //The following comment block is an example of how to load data to the line chart, keeping it in here for reference. :^)
            /*((LineSeries)VisualizedData.Series[0]).ItemsSource =
                new KeyValuePair<DateTime, int>[]{
                new KeyValuePair<DateTime,int>(DateTime.Now, 100),
                new KeyValuePair<DateTime,int>(DateTime.Now.AddMonths(1), 130),
                new KeyValuePair<DateTime,int>(DateTime.Now.AddMonths(2), 150),
                new KeyValuePair<DateTime,int>(DateTime.Now.AddMonths(3), 125),
                new KeyValuePair<DateTime,int>(DateTime.Now.AddMonths(4),155) };*/
        }

        //now that I've gone through this effort, I'm now realizing I could've made this so much more efficiently by calling stats
        //inside of the function so the try-catch would only have to be done once... God, I SERIOUSLY botched this. But, it's...
        //at least functional now. It'll just have to be an optimization-update in the future or something.
        private void Populate_Details(string filter, bool isData)
        {
            if (!isData)
            {
                ExtendedData.Visibility = Visibility.Visible;
                ExtendedData.Header = filter;
                Calculations.Content = "No data for filter(s) selected.";
            }
        }

        private void Maps_Click(object sender, RoutedEventArgs e)
        {
            MapPanel.Visibility = MapPanel.Visibility == Visibility.Collapsed ? Visibility.Visible : Visibility.Collapsed;
            HeroPanel.Visibility = Visibility.Collapsed;
            GroupPanel.Visibility = Visibility.Collapsed;
        }

        private void Heroes_Click(object sender, RoutedEventArgs e)
        {
            MapPanel.Visibility = Visibility.Collapsed;
            HeroPanel.Visibility = HeroPanel.Visibility == Visibility.Collapsed ? Visibility.Visible : Visibility.Collapsed;
            GroupPanel.Visibility = Visibility.Collapsed;
        }

        private void Groups_Click(object sender, RoutedEventArgs e)
        {
            MapPanel.Visibility = Visibility.Collapsed;
            HeroPanel.Visibility = Visibility.Collapsed;
            GroupPanel.Visibility = GroupPanel.Visibility == Visibility.Collapsed ? Visibility.Visible : Visibility.Collapsed;
        }

        private void Ana_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Ana.IsChecked;
            UntoggleHeroes();
            Ana.IsChecked = previousState == false ? false : true;
            if (currentHero == null || currentHero != "Ana")
                currentHero = "Ana";
            else
                currentHero = null;
        }

        private void Bastion_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Bastion.IsChecked;
            UntoggleHeroes();
            Bastion.IsChecked = previousState == false ? false : true;
            if (currentHero == null || currentHero != "Bastion")
                currentHero = "Bastion";
            else
                currentHero = null;
        }

        private void Doomfist_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Doomfist.IsChecked;
            UntoggleHeroes();
            Doomfist.IsChecked = previousState == false ? false : true;
            if (currentHero == null || currentHero != "Doomfist")
                currentHero = "Doomfist";
            else
                currentHero = null;
        }

        private void Dva_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Dva.IsChecked;
            UntoggleHeroes();
            Dva.IsChecked = previousState == false ? false : true;
            if (currentHero == null || currentHero != "Dva")
                currentHero = "Dva";
            else
                currentHero = null;
        }

        private void Genji_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Genji.IsChecked;
            UntoggleHeroes();
            Genji.IsChecked = previousState == false ? false : true;
            if (currentHero == null || currentHero != "Genji")
                currentHero = "Genji";
            else
                currentHero = null;
        }

        private void Hanzo_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Hanzo.IsChecked;
            UntoggleHeroes();
            Hanzo.IsChecked = previousState == false ? false : true;
            if (currentHero == null || currentHero != "Hanzo")
                currentHero = "Hanzo";
            else
                currentHero = null;
        }

        private void Junkrat_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Junkrat.IsChecked;
            UntoggleHeroes();
            Junkrat.IsChecked = previousState == false ? false : true;
            if (currentHero == null || currentHero != "Junk")
                currentHero = "Junk";
            else
                currentHero = null;
        }

        private void Lucio_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Lucio.IsChecked;
            UntoggleHeroes();
            Lucio.IsChecked = previousState == false ? false : true;
            if (currentHero == null || currentHero != "Lucio")
                currentHero = "Lucio";
            else
                currentHero = null;
        }

        private void McCree_Click(object sender, RoutedEventArgs e)
        {
            var previousState = McCree.IsChecked;
            UntoggleHeroes();
            McCree.IsChecked = previousState == false ? false : true;
            if (currentHero == null || currentHero != "McCree")
                currentHero = "McCree";
            else
                currentHero = null;
        }

        private void Mei_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Mei.IsChecked;
            UntoggleHeroes();
            Mei.IsChecked = previousState == false ? false : true;
            if (currentHero == null || currentHero != "Mei")
                currentHero = "Mei";
            else
                currentHero = null;
        }

        private void Mercy_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Mercy.IsChecked;
            UntoggleHeroes();
            Mercy.IsChecked = previousState == false ? false : true;
            if (currentHero == null || currentHero != "Mercy")
                currentHero = "Mercy";
            else
                currentHero = null;
        }

        private void Orisa_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Orisa.IsChecked;
            UntoggleHeroes();
            Orisa.IsChecked = previousState == false ? false : true;
            if (currentHero == null || currentHero != "Orisa")
                currentHero = "Orisa";
            else
                currentHero = null;
        }

        private void Pharah_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Pharah.IsChecked;
            UntoggleHeroes();
            Pharah.IsChecked = previousState == false ? false : true;
            if (currentHero == null || currentHero != "Pharah")
                currentHero = "Pharah";
            else
                currentHero = null;
        }

        private void Reaper_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Reaper.IsChecked;
            UntoggleHeroes();
            Reaper.IsChecked = previousState == false ? false : true;
            if (currentHero == null || currentHero != "Reaper")
                currentHero = "Reaper";
            else
                currentHero = null;
        }

        private void Reinhardt_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Reinhardt.IsChecked;
            UntoggleHeroes();
            Reinhardt.IsChecked = previousState == false ? false : true;
            if (currentHero == null || currentHero != "Rein")
                currentHero = "Rein";
            else
                currentHero = null;
        }

        private void Roadhog_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Roadhog.IsChecked;
            UntoggleHeroes();
            Roadhog.IsChecked = previousState == false ? false : true;
            if (currentHero == null || currentHero != "Road")
                currentHero = "Road";
            else
                currentHero = null;
        }

        private void Soldier76_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Soldier76.IsChecked;
            UntoggleHeroes();
            Soldier76.IsChecked = previousState == false ? false : true;
            if (currentHero == null || currentHero != "Soldier")
                currentHero = "Soldier";
            else
                currentHero = null;
        }

        private void Sombra_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Sombra.IsChecked;
            UntoggleHeroes();
            Sombra.IsChecked = previousState == false ? false : true;
            if (currentHero == null || currentHero != "Sombra")
                currentHero = "Sombra";
            else
                currentHero = null;
        }

        private void Symmetra_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Symmetra.IsChecked;
            UntoggleHeroes();
            Symmetra.IsChecked = previousState == false ? false : true;
            if (currentHero == null || currentHero != "Symm")
                currentHero = "Symm";
            else
                currentHero = null;
        }

        private void Torbjorn_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Torbjorn.IsChecked;
            UntoggleHeroes();
            Torbjorn.IsChecked = previousState == false ? false : true;
            if (currentHero == null || currentHero != "Torbjorn")
                currentHero = "Torbjorn";
            else
                currentHero = null;
        }

        private void Tracer_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Tracer.IsChecked;
            UntoggleHeroes();
            Tracer.IsChecked = previousState == false ? false : true;
            if (currentHero == null || currentHero != "Tracer")
                currentHero = "Tracer";
            else
                currentHero = null;
        }

        private void Widowmaker_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Widowmaker.IsChecked;
            UntoggleHeroes();
            Widowmaker.IsChecked = previousState == false ? false : true;
            if (currentHero == null || currentHero != "Widow")
                currentHero = "Widow";
            else
                currentHero = null;
        }

        private void Winston_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Winston.IsChecked;
            UntoggleHeroes();
            Winston.IsChecked = previousState == false ? false : true;
            if (currentHero == null || currentHero != "Winston")
                currentHero = "Winston";
            else
                currentHero = null;
        }

        private void Zarya_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Zarya.IsChecked;
            UntoggleHeroes();
            Zarya.IsChecked = previousState == false ? false : true;
            if (currentHero == null || currentHero != "Zarya")
                currentHero = "Zarya";
            else
                currentHero = null;
        }

        private void Zenyatta_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Zenyatta.IsChecked;
            UntoggleHeroes();
            Zenyatta.IsChecked = previousState == false ? false : true;
            if (currentHero == null || currentHero != "Zen")
                currentHero = "Zen";
            else
                currentHero = null;
        }

        private void Anubis_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Anubis.IsChecked;
            UntoggleMaps();
            Anubis.IsChecked = previousState == false ? false : true;
            if (currentMap == null || currentMap != "Anubis")
                currentMap = "Anubis";
            else
                currentMap = null;
        }

        private void Dorado_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Dorado.IsChecked;
            UntoggleMaps();
            Dorado.IsChecked = previousState == false ? false : true;
            if (currentMap == null || currentMap != "Dorado")
                currentMap = "Dorado";
            else
                currentMap = null;
        }

        private void Eichenwald_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Eichenwald.IsChecked;
            UntoggleMaps();
            Eichenwald.IsChecked = previousState == false ? false : true;
            if (currentMap == null || currentMap != "Eichenwald")
                currentMap = "Eichenwald";
            else
                currentMap = null;
        }

        private void Gibraltar_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Gibraltar.IsChecked;
            UntoggleMaps();
            Gibraltar.IsChecked = previousState == false ? false : true;
            if (currentMap == null || currentMap != "Gibraltar")
                currentMap = "Gibraltar";
            else
                currentMap = null;
        }

        private void Hanamura_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Hanamura.IsChecked;
            UntoggleMaps();
            Hanamura.IsChecked = previousState == false ? false : true;
            if (currentMap == null || currentMap != "Hanamura")
                currentMap = "Hanamura";
            else
                currentMap = null;
        }

        private void Hollywood_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Hollywood.IsChecked;
            UntoggleMaps();
            Hollywood.IsChecked = previousState == false ? false : true;
            if (currentMap == null || currentMap != "Hollywood")
                currentMap = "Hollywood";
            else
                currentMap = null;
        }

        private void Ilios_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Ilios.IsChecked;
            UntoggleMaps();
            Ilios.IsChecked = previousState == false ? false : true;
            if (currentMap == null || currentMap != "Ilios")
                currentMap = "Ilios";
            else
                currentMap = null;
        }

        private void Junkertown_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Junkertown.IsChecked;
            UntoggleMaps();
            Junkertown.IsChecked = previousState == false ? false : true;
            if (currentMap == null || currentMap != "Junkertown")
                currentMap = "Junkertown";
            else
                currentMap = null;
        }

        private void KingsRow_Click(object sender, RoutedEventArgs e)
        {
            var previousState = KingsRow.IsChecked;
            UntoggleMaps();
            KingsRow.IsChecked = previousState == false ? false : true;
            if (currentMap == null || currentMap != "King")
                currentMap = "King";
            else
                currentMap = null;
        }

        private void Lijiang_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Lijiang.IsChecked;
            UntoggleMaps();
            Lijiang.IsChecked = previousState == false ? false : true;
            if (currentMap == null || currentMap != "Lijiang")
                currentMap = "Lijiang";
            else
                currentMap = null;
        }

        private void Nepal_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Nepal.IsChecked;
            UntoggleMaps();
            Nepal.IsChecked = previousState == false ? false : true;
            if (currentMap == null || currentMap != "Nepal")
                currentMap = "Nepal";
            else
                currentMap = null;
        }

        private void Numbani_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Numbani.IsChecked;
            UntoggleMaps();
            Numbani.IsChecked = previousState == false ? false : true;
            if (currentMap == null || currentMap != "Numbani")
                currentMap = "Numbani";
            else
                currentMap = null;
        }

        private void Oasis_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Oasis.IsChecked;
            UntoggleMaps();
            Oasis.IsChecked = previousState == false ? false : true;
            if (currentMap == null || currentMap != "Oasis")
                currentMap = "Oasis";
            else
                currentMap = null;
        }

        private void Route66_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Route66.IsChecked;
            UntoggleMaps();
            Route66.IsChecked = previousState == false ? false : true;
            if (currentMap == null || currentMap != "Route")
                currentMap = "Route66";
            else
                currentMap = null;
        }

        private void Volskaya_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Volskaya.IsChecked;
            UntoggleMaps();
            Volskaya.IsChecked = previousState == false ? false : true;
            if (currentMap == null || currentMap != "Volskaya")
                currentMap = "Volskaya";
            else
                currentMap = null;
        }

        private void Horizon_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Horizon.IsChecked;
            UntoggleMaps();
            Horizon.IsChecked = previousState == false ? false : true;
            if (currentMap == null || currentMap != "Horizon")
                currentMap = "Horizon";
            else
                currentMap = null;
        }

        private void Solo_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Solo.IsChecked;
            UntoggleGroups();
            Solo.IsChecked = previousState == false ? false : true;
            if (currentGroup == null || currentGroup != "Solo")
                currentGroup = "Solo";
            else
                currentGroup = null;
        }

        private void Duo_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Duo.IsChecked;
            UntoggleGroups();
            Duo.IsChecked = previousState == false ? false : true;
            if (currentGroup == null || currentGroup != "Duo")
                currentGroup = "Duo";
            else
                currentGroup = null;
        }

        private void Trio_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Trio.IsChecked;
            UntoggleGroups();
            Trio.IsChecked = previousState == false ? false : true;
            if (currentGroup == null || currentGroup != "Trio")
                currentGroup = "Trio";
            else
                currentGroup = null;
        }

        private void Quad_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Quad.IsChecked;
            UntoggleGroups();
            Quad.IsChecked = previousState == false ? false : true;
            if (currentGroup == null || currentGroup != "Quad")
                currentGroup = "Quad";
            else
                currentGroup = null;
        }

        private void Fives_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Fives.IsChecked;
            UntoggleGroups();
            Fives.IsChecked = previousState == false ? false : true;
            if (currentGroup == null || currentGroup != "Fives")
                currentGroup = "Fives";
            else
                currentGroup = null;
        }

        private void Six_Click(object sender, RoutedEventArgs e)
        {
            var previousState = Six.IsChecked;
            UntoggleGroups();
            Six.IsChecked = previousState == false ? false : true;
            if (currentGroup == null || currentGroup != "Six")
                currentGroup = "Six";
            else
                currentGroup = null;
        }

        private void UntoggleHeroes()
        {
            if (Ana.IsChecked == true)
            {
                Ana.IsChecked = false;
            }
            if (Bastion.IsChecked == true)
            {
                Bastion.IsChecked = false;
            }
            if (Doomfist.IsChecked == true)
            {
                Doomfist.IsChecked = false;
            }
            if (Dva.IsChecked == true)
            {
                Dva.IsChecked = false;
            }
            if (Genji.IsChecked == true)
            {
                Genji.IsChecked = false;
            }
            if (Hanzo.IsChecked == true)
            {
                Hanzo.IsChecked = false;
            }
            if (Junkrat.IsChecked == true)
            {
                Junkrat.IsChecked = false;
            }
            if (Lucio.IsChecked == true)
            {
                Lucio.IsChecked = false;
            }
            if (McCree.IsChecked == true)
            {
                McCree.IsChecked = false;
            }
            if (Mei.IsChecked == true)
            {
                Mei.IsChecked = false;
            }
            if (Mercy.IsChecked == true)
            {
                Mercy.IsChecked = false;
            }
            if (Orisa.IsChecked == true)
            {
                Orisa.IsChecked = false;
            }
            if (Pharah.IsChecked == true)
            {
                Pharah.IsChecked = false;
            }
            if (Reaper.IsChecked == true)
            {
                Reaper.IsChecked = false;
            }
            if (Reinhardt.IsChecked == true)
            {
                Reinhardt.IsChecked = false;
            }
            if (Roadhog.IsChecked == true)
            {
                Roadhog.IsChecked = false;
            }
            if (Soldier76.IsChecked == true)
            {
                Soldier76.IsChecked = false;
            }
            if (Sombra.IsChecked == true)
            {
                Sombra.IsChecked = false;
            }
            if (Symmetra.IsChecked == true)
            {
                Symmetra.IsChecked = false;
            }
            if (Torbjorn.IsChecked == true)
            {
                Torbjorn.IsChecked = false;
            }
            if (Tracer.IsChecked == true)
            {
                Tracer.IsChecked = false;
            }
            if (Widowmaker.IsChecked == true)
            {
                Widowmaker.IsChecked = false;
            }
            if (Winston.IsChecked == true)
            {
                Winston.IsChecked = false;
            }
            if (Zarya.IsChecked == true)
            {
                Zarya.IsChecked = false;
            }
            if (Zenyatta.IsChecked == true)
            {
                Zenyatta.IsChecked = false;
            }
        }

        private void UntoggleMaps()
        {
            if (Anubis.IsChecked == true)
            {
                Anubis.IsChecked = false;
            }
            if (Dorado.IsChecked == true)
            {
                Dorado.IsChecked = false;
            }
            if (Eichenwald.IsChecked == true)
            {
                Eichenwald.IsChecked = false;
            }
            if (Gibraltar.IsChecked == true)
            {
                Gibraltar.IsChecked = false;
            }
            if (Hanamura.IsChecked == true)
            {
                Hanamura.IsChecked = false;
            }
            if (Hollywood.IsChecked == true)
            {
                Hollywood.IsChecked = false;
            }
            if (Horizon.IsChecked == true)
            {
                Horizon.IsChecked = false;
            }
            if (Ilios.IsChecked == true)
            {
                Ilios.IsChecked = false;
            }
            if (Junkertown.IsChecked == true)
            {
                Junkertown.IsChecked = false;
            }
            if (KingsRow.IsChecked == true)
            {
                KingsRow.IsChecked = false;
            }
            if (Lijiang.IsChecked == true)
            {
                Lijiang.IsChecked = false;
            }
            if (Nepal.IsChecked == true)
            {
                Nepal.IsChecked = false;
            }
            if (Numbani.IsChecked == true)
            {
                Numbani.IsChecked = false;
            }
            if (Oasis.IsChecked == true)
            {
                Oasis.IsChecked = false;
            }
            if (Route66.IsChecked == true)
            {
                Route66.IsChecked = false;
            }
            if (Volskaya.IsChecked == true)
            {
                Volskaya.IsChecked = false;
            }
        }

        private void UntoggleGroups()
        {
            if (Solo.IsChecked == true)
            {
                Solo.IsChecked = false;
            }
            if (Duo.IsChecked == true)
            {
                Duo.IsChecked = false;
            }
            if (Trio.IsChecked == true)
            {
                Trio.IsChecked = false;
            }
            if (Quad.IsChecked == true)
            {
                Quad.IsChecked = false;
            }
            if (Fives.IsChecked == true)
            {
                Fives.IsChecked = false;
            }
            if (Six.IsChecked == true)
            {
                Six.IsChecked = false;
            }
        }

        private void StartCalc_Click(object sender, RoutedEventArgs e)
        {
            if(currentHero == null && currentMap == null && currentGroup == null)
            {
                //is this a valid case?
            }
            else if (currentHero != null && currentMap == null && currentGroup == null)
            {
                try
                {
                    var results = stats.Calculate(currentHero, "Hero");
                    currentHero = DetermineActualHeroName(currentHero);
                    Populate_Details(currentHero, results);
                }
                catch
                {
                    currentHero = DetermineActualHeroName(currentHero);
                    Populate_Details(currentHero, false);
                }
            }
            if (currentHero == null && currentMap != null && currentGroup == null)
            {
                try
                {
                    var results = stats.Calculate(currentMap, "Map");
                    currentMap = DetermineActualMapName(currentMap);
                    Populate_Details(currentMap, results);
                }
                catch
                {
                    currentMap = DetermineActualMapName(currentMap);
                    Populate_Details(currentMap, false);
                }
            }
            if (currentHero == null && currentMap == null && currentGroup != null)
            {
                try
                {
                    var results = stats.Calculate(currentGroup, "Group");
                    Populate_Details(currentGroup, results);
                }
                catch
                {
                    Populate_Details(currentGroup, false);
                }
            }
            if (currentHero != null && currentMap != null && currentGroup == null)
            {
                try
                {
                    var results = stats.Calculate(currentHero, "Hero", currentMap, "Map");
                    currentHero = DetermineActualHeroName(currentHero);
                    currentMap = DetermineActualMapName(currentMap);
                    Populate_Details(currentHero + " + " + currentMap, results);
                }
                catch
                {
                    currentHero = DetermineActualHeroName(currentHero);
                    currentMap = DetermineActualMapName(currentMap);
                    Populate_Details(currentHero + " + " + currentMap, false);
                }
            }
            if (currentHero != null && currentMap == null && currentGroup != null)
            {
                try
                {
                    var results = stats.Calculate(currentHero, "Hero", currentGroup, "Group");
                    currentHero = DetermineActualHeroName(currentHero);
                    Populate_Details(currentHero + " + " + currentGroup, results);
                }
                catch
                {
                    currentHero = DetermineActualHeroName(currentHero);
                    Populate_Details(currentHero + " + " + currentGroup, false);
                }
            }
            if (currentHero == null && currentMap != null && currentGroup != null)
            {
                try
                {
                    var results = stats.Calculate(currentGroup, "Group", currentMap, "Map");
                    currentMap = DetermineActualMapName(currentMap);
                    Populate_Details(currentGroup + " + " + currentMap, results);
                }
                catch
                {
                    currentMap = DetermineActualMapName(currentMap);
                    Populate_Details(currentGroup + " + " + currentMap, false);
                }
            }
            if (currentHero != null && currentMap != null && currentGroup != null)
            {
                try
                {
                    var results = stats.Calculate(currentHero, "Hero", currentMap, "Map", currentGroup, "Group");
                    currentHero = DetermineActualHeroName(currentHero);
                    currentMap = DetermineActualMapName(currentMap);
                    Populate_Details(currentHero + " + " + currentMap + " + " + currentGroup, results);
                }
                catch
                {
                    currentHero = DetermineActualHeroName(currentHero);
                    currentMap = DetermineActualMapName(currentMap);
                    Populate_Details(currentHero + " + " + currentMap + " + " + currentGroup, false);
                }
            }
        }

        private string DetermineActualHeroName(string abbreviated)
        {
            if (abbreviated.Contains("Dva"))
                return "D.Va";
            else if (abbreviated.Contains("Junk"))
                return "Junkrat";
            else if (abbreviated.Contains("Rein"))
                return "Reinhardt";
            else if (abbreviated.Contains("Road"))
                return "Roadhog";
            else if (abbreviated.Contains("Soldier"))
                return "Soldier: 76";
            else if (abbreviated.Contains("Sym"))
                return "Symmetra";
            else if (abbreviated.Contains("Torb"))
                return "Torbjorn";
            else if (abbreviated.Contains("Widow"))
                return "Widowmaker";
            else if (abbreviated.Contains("Zen"))
                return "Zenyatta";
            else
                return abbreviated;     //this means it was not abbreviated at any point :^)
        }

        private string Reabbreviate(string elongated)
        {
            if (elongated.Contains("Anubis"))
                return "Anubis";
            else if (elongated.Contains("Eichenwald"))
                return "Eichenwald";
            else if (elongated.Contains("Gibraltar"))
                return "Gibraltar";
            else if (elongated.Contains("King"))
                return "King";
            else if (elongated.Contains("Lijiang"))
                return "Lijiang";
            else if (elongated.Contains("Route"))
                return "Route";
            else if (elongated.Contains("Horizon"))
                return "Horizon";
            else if (elongated=="D.Va")
                return "Dva";
            else if (elongated.Contains("Junk"))
                return "Junk";
            else if (elongated.Contains("Rein"))
                return "Rein";
            else if (elongated.Contains("Road"))
                return "Road";
            else if (elongated.Contains("Soldier"))
                return "Soldier";
            else if (elongated.Contains("Sym"))
                return "Symm";
            else if (elongated.Contains("Torb"))
                return "Torb";
            else if (elongated.Contains("Widow"))
                return "Widow";
            else if (elongated.Contains("Zen"))
                return "Zen";
            else
                return elongated;     //this means it was not abbreviated at any point :^)
        }

        private string DetermineActualMapName(string abbreviated)
        {
            if (abbreviated.Contains("Anubis"))
                return "Temple of Anubis";
            else if (abbreviated.Contains("Dorado"))
                return "Dorado";
            else if (abbreviated.Contains("Eichenwald"))
                return "Eichenwalde";
            else if (abbreviated.Contains("Gibraltar"))
                return "Watchpoint: Gibraltar";
            else if (abbreviated.Contains("Hanamura"))
                return "Hanamura";
            else if (abbreviated.Contains("Hollywood"))
                return "Hollywood";
            else if (abbreviated.Contains("Ilios"))
                return "Ilios";
            else if (abbreviated.Contains("King"))
                return "King's Row";
            else if (abbreviated.Contains("Lijiang"))
                return "Lijiang Tower";
            else if (abbreviated.Contains("Nepal"))
                return "Nepal";
            else if (abbreviated.Contains("Numbani"))
                return "Numbani";
            else if (abbreviated.Contains("Oasis"))
                return "Oasis";
            else if (abbreviated.Contains("Route"))
                return "Route 66";
            else if (abbreviated.Contains("Volskaya"))
                return "Volskaya";
            else if (abbreviated.Contains("Horizon"))
                return "Horizon Lunar Colony";
            else
                return abbreviated;   //this shouldn't realistically ever be returned, but it doesn't hurt to have the case there.
        }
    }
}
