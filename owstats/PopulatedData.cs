﻿namespace owstats
{
    public class PopulatedData
    {
        int gameCount;
        int calibratedSR;
        int currentSR;
        int winCount;
        int lossCount;
        int tieCount;

        public PopulatedData()
        {
            gameCount = 0;
            calibratedSR = 0;
            currentSR = 0;
            winCount = 0;
            lossCount = 0;
            tieCount = 0;
        }

        public int getCalibratedSR()
        {
            return calibratedSR;
        }

        public int getCurrentSR()
        {
            return currentSR;
        }

        public int getWins()
        {
            return winCount;
        }

        public int getLosses()
        {
            return lossCount;
        }

        public int getTies()
        {
            return tieCount;
        }

        public int getGames()
        {
            return gameCount;
        }

        public void setCalibratedSR(int _calibratedSR)
        {
            calibratedSR = _calibratedSR;
        }

        public void setCurrentSR(int _currentSR)
        {
            currentSR = _currentSR;
        }

        public void setWins(int _wins)
        {
            winCount = _wins;
        }

        public void setLosses(int _loss)
        {
            lossCount = _loss;
        }

        public void setTies(int _ties)
        {
            tieCount = _ties;
        }

        public void setGames(int _games)
        {
            gameCount = _games;
        }

        public void addWin()
        {
            winCount++;
            gameCount++;
        }

        public void addLoss()
        {
            lossCount++;
            gameCount++;
        }

        public void addTie()
        {
            tieCount++;
            gameCount++;
        }
    }
}
