﻿using System;

namespace owstats
{
    public class Game
    {
        int SR;
        int groupSize;
        string type;
        string map;
        string[] heroesPlayed;
        DateTime whenPlayed;
        public string outcome;

        public Game(int _SR, int _groupSize, string _type, string _map, string[] _heroesPlayed, DateTime _whenPlayed, string _outcome)
        {
            this.SR = _SR;
            this.groupSize = _groupSize;
            this.type = _type;
            this.map = _map;
            this.heroesPlayed = _heroesPlayed;
            this.whenPlayed = _whenPlayed;
            this.outcome = _outcome;
        }

        public Game(int _SR, int _groupSize, string _type, string _map, string[] _heroesPlayed, DateTime _whenPlayed)
        {
            this.SR = _SR;
            this.groupSize = _groupSize;
            this.type = _type;
            this.map = _map;
            this.heroesPlayed = _heroesPlayed;
            this.whenPlayed = _whenPlayed;
        }

        public void WasWin()
        {
            outcome = "Win";
        }

        public void WasLoss()
        {
            outcome = "Loss";
        }

        public void WasTie()
        {
            outcome = "Tie";
        }

        public int GetSR()
        {
            return this.SR;
        }

        public int GetGroupSize()
        {
            return this.groupSize;
        }

        public string GetMatchType()
        {
            return this.type;
        }

        public string GetMap()
        {
            return this.map;
        }

        public string[] GetHeroes()
        {
            return this.heroesPlayed;
        }

        public DateTime GetTime()
        {
            return this.whenPlayed;
        }

        public string GetGame()
        {
            string details = this.SR.ToString() + "," + this.groupSize.ToString() + "," + this.type.ToString() + ","
                + this.map.ToString() + ",";

            foreach(string hero in heroesPlayed)
            {
                if (heroesPlayed[heroesPlayed.Length - 1] == hero)
                    details = details + hero;
                else
                    details = details + hero + "|";
            }

            details = details + "," + whenPlayed + "," + outcome;

            return details;
        }
    }
}
