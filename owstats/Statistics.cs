﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace owstats
{
    public class Statistics
    {
        public decimal winrate;
        public decimal winrateWithTies;
        public int winCount;
        public int lossCount;
        public int tieCount;

        public Statistics(ArrayList listOfGames)
        {
            foreach(Game match in listOfGames)
            {
                if (match.outcome == "Win")
                    winCount++;
                else if (match.outcome == "Tie")
                    tieCount++;
                else
                    lossCount++;
            }

            winrate = (decimal)winCount / (winCount + lossCount);
            winrateWithTies = (decimal)winCount / (winCount + lossCount + tieCount);
        }
    }
}
