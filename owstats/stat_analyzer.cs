﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace owstats
{
    class Stat_analyzer
    {
        PopulatedData existingData = new PopulatedData();
        string seasonGamesLocation = Directory.GetCurrentDirectory() + "/owstats.games";
        ArrayList allGames;
        ArrayList filteredGames;
        bool loaded = false;

        private readonly FileInfo _gamesStorage = new FileInfo(Path.Combine(Directory.GetCurrentDirectory(), "owstats.games"));

        //This is gonna be a relatively large project, so therefore... noty right now
        /*public ArrayList CalculateAll(string type)
        {
            ArrayList heroList = new ArrayList() { "Ana", "Bastion", "Dva", "Genji", "Hanzo", "Junk", "Lucio", "McCree", "Mei", "Mercy", "Orisa",
                                                    "Pharah", "Reaper", "Rein", "Road", "Soldier", "Sombra", "Sym", "Torb",
                                                    "Tracer", "Widow", "Winston", "Zarya", "Zen"};
            ArrayList mapList = new ArrayList() { "anubis", "dorado", "eichenwald", "gibraltar", "hanamura", "hollywood", "ilios", "kings row",
                                                    "lijiang", "nepal", "numbani", "oasis", "route", "volskaya" };
            ArrayList groupList = new ArrayList() { "1", "2", "3", "4", "5", "6" };
            ArrayList mapTypeList = new ArrayList() { "Assault", "Control Point", "Hybrid", "Payload" };

            if (!loaded)
                LoadData(_gamesStorage);
            filteredGames = new ArrayList();

            if(type == "Hero")
            {
                foreach (string filter in heroList)
                {
                    if (match.getHeroes().Contains(filter)) //then, we check if the typed data matches the filter
                        filteredGames.Add(match);
                }
            }
            if (type == "Map")
            {

            }

            if (type == "Group")
            {

            }
            
            if (type == "Map Type")
            {

            }

            foreach (Game match in allGames)
            {
                //first, we check what type of data we're looking to narrow by
                if (type == "Hero")
                {
                    filteredGames.Add(match);
                }
                else if (type == "Map")
                {
                    filteredGames.Add(match);
                }
                else if (type == "Group")
                {
                    filteredGames.Add(match);
                }
            }
            var allInCategory = new ArrayList();

            Statistics calculator = new Statistics(filteredGames);

            return calculator;
        }*/

        public ArrayList AccessFilteredGames()
        {
            return filteredGames;
        }

        public Statistics Calculate(string filter, string type)
        {
            if(!loaded)
                LoadData(_gamesStorage);
            filteredGames = new ArrayList();

            foreach(Game match in allGames)
            {
                //first, we check what type of data we're looking to narrow by
                if(type == "Hero")
                {
                    if (match.GetHeroes().Contains(filter)) //then, we check if the typed data matches the filter
                        filteredGames.Add(match);
                }
                else if (type == "Map")
                {
                    if (match.GetMap().Contains(filter))
                    {
                        filteredGames.Add(match);
                    }
                }
                else if(type == "Group")
                {
                    if(match.GetGroupSize().ToString() == filter)
                    {
                        filteredGames.Add(match);
                    }
                }
            }

            Statistics calculator = new Statistics(filteredGames);

            return calculator;
        }

        public Statistics Calculate(string filter1, string type1, string filter2, string type2)
        {
            if (!loaded)
                LoadData(_gamesStorage);
            filteredGames = new ArrayList();

            foreach (Game match in allGames)
            {
                //first, we check what type of data we're looking to narrow by
                if (type1 == "Hero" || type2 == "Hero")
                {
                    if (match.GetHeroes().Contains(filter1))
                    {
                        if (match.GetMap().Contains(filter2) || match.GetGroupSize().ToString() == filter2)
                            filteredGames.Add(match);
                    }
                    else if(match.GetHeroes().Contains(filter2))
                    { 
                        if(match.GetMap().Contains(filter1) || match.GetGroupSize().ToString() == filter1)
                            filteredGames.Add(match);
                    }
                }
                else if (type1 == "Map" || type2 == "Map")
                {
                    if (match.GetMap().Contains(filter1))
                    {
                        if (match.GetHeroes().Contains(filter2) || match.GetGroupSize().ToString() == filter2)
                            filteredGames.Add(match);
                    }
                    else if (match.GetMap().Contains(filter2))
                    {
                        if (match.GetHeroes().Contains(filter1) || match.GetGroupSize().ToString() == filter1)
                            filteredGames.Add(match);
                    }
                }
                else if (type1 == "Group" || type2 == "Group")
                {
                    if (match.GetGroupSize().ToString() == filter1)
                    {
                        if(match.GetHeroes().Contains(filter2) || match.GetMap().Contains(filter2))
                            filteredGames.Add(match);
                    }
                    else if (match.GetGroupSize().ToString() == filter2)
                    {
                        if (match.GetHeroes().Contains(filter1) || match.GetMap().Contains(filter1))
                            filteredGames.Add(match);
                    }
                }
            }

            Statistics calculator = new Statistics(filteredGames);

            return calculator;
        }

        public Statistics Calculate(string filter1, string type1, string filter2, string type2, string filter3, string type3)
        {
            if (!loaded)
                LoadData(_gamesStorage);
            filteredGames = new ArrayList();

            foreach (Game match in allGames)
            {
                if(type1 == "Hero" && type2 == "Map" && type3 == "Group")
                {
                    if(match.GetHeroes().Contains(filter1) && match.GetMap().Contains(filter2) && match.GetGroupSize().ToString() == filter3)
                    {
                        filteredGames.Add(match);
                    }
                }
                if (type1 == "Hero" && type2 == "Group" && type3 == "Map")
                {
                    if (match.GetHeroes().Contains(filter1) && match.GetMap().Contains(filter3) && match.GetGroupSize().ToString() == filter2)
                    {
                        filteredGames.Add(match);
                    }
                }
                if (type1 == "Map" && type2 == "Hero" && type3 == "Group")
                {
                    if (match.GetHeroes().Contains(filter2) && match.GetMap().Contains(filter1) && match.GetGroupSize().ToString() == filter3)
                    {
                        filteredGames.Add(match);
                    }
                }
                if (type1 == "Map" && type2 == "Group" && type3 == "Hero")
                {
                    if (match.GetHeroes().Contains(filter3) && match.GetMap().Contains(filter1) && match.GetGroupSize().ToString() == filter2)
                    {
                        filteredGames.Add(match);
                    }
                }
                if (type1 == "Group" && type2 == "Map" && type3 == "Hero")
                {
                    if (match.GetHeroes().Contains(filter3) && match.GetMap().Contains(filter2) && match.GetGroupSize().ToString() == filter1)
                    {
                        filteredGames.Add(match);
                    }
                }
                if (type1 == "Group" && type2 == "Hero" && type3 == "Map")
                {
                    if (match.GetHeroes().Contains(filter2) && match.GetMap().Contains(filter3) && match.GetGroupSize().ToString() == filter1)
                    {
                        filteredGames.Add(match);
                    }
                }
            }

            Statistics calculator = new Statistics(filteredGames);

            return calculator;
        }

        public bool LoadData(FileInfo gamesStorage)
        {
            allGames = new ArrayList();

            if (gamesStorage.Exists == false) //if the gamesStorage file doesn't exist, create it
            {
                using (var writer = gamesStorage.OpenWrite())
                {
                    writer.Close();
                }
            }
            else //otherwise, parse the data from it.
            {
                using (var reader = gamesStorage.OpenText())
                {
                    ArrayList games = new ArrayList();
                    while (reader.EndOfStream == false)
                    {
                        games.Add(reader.ReadLine());
                    }

                    foreach (string game in games)
                    {
                        //sr, groupsize, type, map, heroes played, time
                        var individualGame = game.Split(',');
                        var SR = Int32.Parse(individualGame[0]);
                        var groupSize = Int32.Parse(individualGame[1]);
                        var type = individualGame[2];
                        var map = individualGame[3];
                        var heroesPlayed = individualGame[4];
                        var heroList = heroesPlayed.Split('|'); //split up the list to pass it to game properly
                        var timePlayed = DateTime.Parse(individualGame[5]);
                        var outcome = individualGame[6];

                        Game oldGame = new Game(SR, groupSize, type, map, heroList, timePlayed, outcome);
                        allGames.Add(oldGame);
                    }
                    reader.Close();
                }
            }

            loaded = true;
            return true;
        }
    }
}
