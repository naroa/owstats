﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using System.IO;
using System.Collections;

namespace owstats
{

    public class Stat_tracker
    {
        public PopulatedData existingData = new PopulatedData();
        string seasonGamesLocation = Directory.GetCurrentDirectory() + "/owstats.games";
        string savedDataLocation = Directory.GetCurrentDirectory() + "/owstats.data";
        ArrayList seasonGames;
        bool loaded = false;
        public bool isCalibrating = true;
        public int differenceSR = 0;

        private readonly FileInfo _dataStorage = new FileInfo(Path.Combine(Directory.GetCurrentDirectory(), "/owstats.data"));

        public void StatTrackTester()
        {
            string[] _heroes = { "Widowmaker"};
            StatTrack(3000, 2, "Hybrid", "Eichenwald", _heroes, DateTime.Now);
        }

        //StatTrack will be called by MainWindow.xaml.cs to do calculations. Input params will be provided
        public void StatTrack(int newSR, int groupSize, string type, string map, string[] heroes, DateTime timePlayed)
        {
            //load previous data
            if(!loaded)
                LoadData(new FileInfo(savedDataLocation), new FileInfo(seasonGamesLocation));

            //can be calculated at request
            int YTDdifferenceSR = 0;
            
            //open window, receive game data
            Game latestGame = new Game(newSR, groupSize, type, map, heroes, timePlayed);

            newSR = latestGame.GetSR();

            //do calcuations on SR difference to determine W/L and run forward with stats
            YTDdifferenceSR = newSR - existingData.getCalibratedSR() ;
            differenceSR = newSR - existingData.getCurrentSR();
            if (differenceSR > 0)
            {
                existingData.addWin();
                latestGame.WasWin();
            }
            else if (differenceSR < 0)
            {
                existingData.addLoss();
                latestGame.WasLoss();
            }                
            else
            {
                existingData.addTie();
                latestGame.WasTie();
            }

            //after all is said and done and we're ready to receive next game
            seasonGames.Add(latestGame);
            SaveGame(latestGame);
            existingData.setCurrentSR(newSR);
            SaveData();
        }

        //should probably split up the loading of the compiled data and the game info for testing purposes, but this will do for now
        public bool LoadData(FileInfo dataStorage, FileInfo gamesStorage)
        {
            if (dataStorage.Exists == false)
            {
                using (var writer = dataStorage.OpenWrite())
                {
                    writer.Close();
                }
            }
            else
            {
                using (var reader = dataStorage.OpenText())
                {
                    if (reader.EndOfStream == false)
                    {
                        //might be better to use TryParse instead of parse, but in theory it shouldn't be necessary
                        var parsedInteger = Int32.Parse(reader.ReadLine()); //parses out Game count
                        existingData.setGames(parsedInteger);
                        parsedInteger = Int32.Parse(reader.ReadLine()); //parses out Calibrated SR
                        existingData.setCalibratedSR(parsedInteger);
                        parsedInteger = Int32.Parse(reader.ReadLine()); //parses out Current SR
                        existingData.setCurrentSR(parsedInteger);
                        parsedInteger = Int32.Parse(reader.ReadLine()); //parses out Wins
                        existingData.setWins(parsedInteger);
                        parsedInteger = Int32.Parse(reader.ReadLine()); //parses out Ties
                        existingData.setTies(parsedInteger);
                        parsedInteger = Int32.Parse(reader.ReadLine()); //parses out Losses
                        existingData.setLosses(parsedInteger);
                        
                        reader.Close();
                    }
                    else
                        return false;
                }
            }
            
            seasonGames = new ArrayList();

            if (gamesStorage.Exists == false) //if the gamesStorage file doesn't exist, create it
            {
                using (var writer = gamesStorage.OpenWrite())
                {
                    writer.Close();
                }
            }
            else //otherwise, parse the data from it.
            {
                using (var reader = gamesStorage.OpenText())
                {
                    ArrayList games = new ArrayList();
                    while (reader.EndOfStream == false)
                    {
                        games.Add(reader.ReadLine());
                    }
                    
                    foreach(string game in games)
                    {
                        //sr, groupsize, type, map, heroes played, time
                        var individualGame = game.Split(',');
                        var SR = Int32.Parse(individualGame[0]);
                        var groupSize = Int32.Parse(individualGame[1]);
                        var type = individualGame[2];
                        var map = individualGame[3];
                        var heroesPlayed = individualGame[4]; 
                        var heroList = heroesPlayed.Split('|'); //split up the list to pass it to game properly
                        var timePlayed = DateTime.Parse(individualGame[5]);
                        var outcome = individualGame[6];

                        Game oldGame = new Game(SR, groupSize, type, map, heroList, timePlayed, outcome);
                        seasonGames.Add(oldGame);
                    }
                    reader.Close();
                }
            }

            //check to see how many games are in the season to determine if still calibrating
            if(seasonGames.Count<10)
                isCalibrating = true;
            else
                isCalibrating = false;
            //with the above we can decide what requirements are needed to determine result... Maybe instead of taking in SR
            //it takes in a boolean question for W/L? this will ABSOLUTELY require a retooling of some data structures though
            
            loaded = true; //setting this to true to avoid loading unnecessarily :^)
            return true;
        }

        public bool SaveData()
        {
            //rewrite savedData
            try
            {
                using (StreamWriter writer = File.CreateText(savedDataLocation))
                {
                    writer.WriteLine(existingData.getGames().ToString());
                    writer.WriteLine(existingData.getCalibratedSR().ToString());
                    writer.WriteLine(existingData.getCurrentSR().ToString());
                    writer.WriteLine(existingData.getWins().ToString());
                    writer.WriteLine(existingData.getTies().ToString());
                    writer.WriteLine(existingData.getLosses().ToString());
                    writer.Close();
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool SaveGame(Game gameData)
        {
            //append gameData to seasonData
            try
            {
                using (StreamWriter writer = new StreamWriter(seasonGamesLocation, true)) //this syntax allows me to append the game's data
                {
                    writer.WriteLine(gameData.GetGame());
                    writer.Close();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
