﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace owstats
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Stat_tracker trackTheStats = new Stat_tracker();
        string seasonGamesLocation = Directory.GetCurrentDirectory() + "/owstats.games";
        string savedDataLocation = Directory.GetCurrentDirectory() + "/owstats.data";
        string[] groupSizesOptions = new string[] { "1", "2", "3", "4", "5", "6" };
        string[] mapOptions = new string[] {"Temple of Anubis", "Hanamura",  "Horizon Lunar Colony", "Volskaya Industries", "Ilios", "Nepal", "Oasis", "Lijiang Tower", "Dorado", "Junkertown", "Watchpoint: Gibraltar", "Route 66", "Eichenwald",
            "Hollywood", "King's Row", "Numbani"};
        string[] heroAllowedNames = new string[] {"Ana", "Bastion", "Doomfist", "Dva", "Genji", "Hanzo", "Junkrat", "Lucio", "McCree", "Mei", "Mercy", "Orisa", "Pharah", "Reaper", "Reinhardt", "Roadhog", "Soldier76",
            "Sombra", "Symmetra", "Tracer", "Torbjorn", "Widowmaker", "Winston", "Zarya", "Zenyatta"};
        string[] heroOptions = new string[] {"Ana", "Bastion", "Doomfist", "D.Va", "Genji", "Hanzo", "Junkrat", "Lucio", "McCree", "Mei", "Mercy", "Orisa", "Pharah", "Reaper", "Reinhardt", "Roadhog", "Soldier: 76",
            "Sombra", "Symmetra", "Tracer", "Torbjorn", "Widowmaker", "Winston", "Zarya", "Zenyatta"};

        public MainWindow()
        {
            InitializeComponent();
            foreach(string option in groupSizesOptions)
                GroupsDropdown.Items.Add(option);
            foreach (string option in mapOptions)
                MapsDropdown.Items.Add(option);
            PopulateHeroes();
            trackTheStats.LoadData(new FileInfo(savedDataLocation), new FileInfo(seasonGamesLocation));
            if (trackTheStats.isCalibrating)
            {
                //display calibration buttons
                SR.Visibility = Visibility.Hidden;
                Win.Visibility = Visibility.Visible;
                Loss.Visibility = Visibility.Visible;
                Tie.Visibility = Visibility.Visible;
            }
            else
            {
                //display normal case
                SR.Visibility = Visibility.Visible;
                Win.Visibility = Visibility.Hidden;
                Loss.Visibility = Visibility.Hidden;
                Tie.Visibility = Visibility.Hidden;
            }
        }

        private string DetermineMapType(string _map)
        {
            if (_map.ToLower().Contains("anubis") || _map.ToLower().Contains("hanamura") || _map.ToLower().Contains("volskaya") || _map.ToLower().Contains("horizon"))
            {
                return "Assault";
            }
            else if (_map.ToLower().Contains("ilios") || _map.ToLower().Contains("nepal") || _map.ToLower().Contains("oasis") ||
                _map.ToLower().Contains("lijiang"))
            {
                return "Capture Point";
            }
            else if (_map.ToLower().Contains("dorado") || (_map.ToLower().Contains("junkertown")) || _map.ToLower().Contains("gibraltar") || _map.ToLower().Contains("route"))
            {
                return "Payload";
            }
            else if (_map.ToLower().Contains("eichenwald") || _map.ToLower().Contains("hollywood") || _map.ToLower().Contains("king") ||
                _map.ToLower().Contains("numbani"))
            {
                return "Hybrid";
            }
            else
                return "INVALID MAP";
        }

        private void AddGameToDatabase(object sender, RoutedEventArgs e)
        {
            var heroes = CheckHeroesSelected();
            var heroesPlayed = heroes.Split('|');
            try
            {
                if (HeroesDropdown.SelectedIndex == 0 || MapsDropdown.SelectedIndex == -1 || GroupsDropdown.SelectedIndex == -1)
                    throw (new Exception());
                if (trackTheStats.existingData.getGames() == 0)
                {
                    if(Win.IsChecked == true)
                    {
                        trackTheStats.StatTrack(11, Int32.Parse(GroupsDropdown.SelectedItem.ToString()), DetermineMapType(MapsDropdown.SelectedItem.ToString()), MapsDropdown.SelectedItem.ToString(), heroesPlayed, DateTime.Now);
                    }
                    else if(Loss.IsChecked == true)
                    {
                        trackTheStats.StatTrack(9, Int32.Parse(GroupsDropdown.SelectedItem.ToString()), DetermineMapType(MapsDropdown.SelectedItem.ToString()), MapsDropdown.SelectedItem.ToString(), heroesPlayed, DateTime.Now);
                    }
                    else if(Tie.IsChecked == true)
                    {
                        trackTheStats.StatTrack(10, Int32.Parse(GroupsDropdown.SelectedItem.ToString()), DetermineMapType(MapsDropdown.SelectedItem.ToString()), MapsDropdown.SelectedItem.ToString(), heroesPlayed, DateTime.Now);
                    }
                    else
                    {
                        throw (new Exception());
                    }
                }
                else if(trackTheStats.existingData.getGames() < 10){
                    if (Win.IsChecked == true)
                    {
                        trackTheStats.StatTrack(trackTheStats.existingData.getCurrentSR() + 1, Int32.Parse(GroupsDropdown.SelectedItem.ToString()),
                            DetermineMapType(MapsDropdown.SelectedItem.ToString()), MapsDropdown.SelectedItem.ToString(), heroesPlayed, DateTime.Now);
                        if (trackTheStats.existingData.getGames() == 10)
                        {
                            Win.Visibility = Visibility.Collapsed;
                            Loss.Visibility = Visibility.Collapsed;
                            Tie.Visibility = Visibility.Collapsed;
                            SR.Visibility = Visibility.Visible;
                            DetermineCalibratedSR();
                        }
                    }
                    else if (Loss.IsChecked == true)
                    {
                        trackTheStats.StatTrack(trackTheStats.existingData.getCurrentSR() - 1, Int32.Parse(GroupsDropdown.SelectedItem.ToString()),
                            DetermineMapType(MapsDropdown.SelectedItem.ToString()), MapsDropdown.SelectedItem.ToString(), heroesPlayed, DateTime.Now);
                        if (trackTheStats.existingData.getGames() == 10)
                        {
                            Win.Visibility = Visibility.Collapsed;
                            Loss.Visibility = Visibility.Collapsed;
                            Tie.Visibility = Visibility.Collapsed;
                            SR.Visibility = Visibility.Visible;
                            DetermineCalibratedSR();
                        }
                    }
                    else if (Tie.IsChecked == true)
                    {
                        trackTheStats.StatTrack(trackTheStats.existingData.getCurrentSR(), Int32.Parse(GroupsDropdown.SelectedItem.ToString()),
                            DetermineMapType(MapsDropdown.SelectedItem.ToString()), MapsDropdown.SelectedItem.ToString(), heroesPlayed, DateTime.Now);
                        if (trackTheStats.existingData.getGames() == 10)
                        {
                            Win.Visibility = Visibility.Collapsed;
                            Loss.Visibility = Visibility.Collapsed;
                            Tie.Visibility = Visibility.Collapsed;
                            SR.Visibility = Visibility.Visible;
                            DetermineCalibratedSR();
                        }
                    }
                    else
                    {
                        throw(new Exception());
                    }
                }
                else
                {
                    trackTheStats.StatTrack(Int32.Parse(SR.Text), Int32.Parse(GroupsDropdown.SelectedItem.ToString()), DetermineMapType(MapsDropdown.SelectedItem.ToString()), MapsDropdown.SelectedItem.ToString(), heroesPlayed, DateTime.Now);
                }
                //once we've clicked the button, we want to wipe the slate clean on the text boxes for multiple uses :^)
                AddMostRecentGameToScreen();
                Win.IsChecked = false;
                Loss.IsChecked = false;
                Tie.IsChecked = false;
                SR.Text = "SR";
                MapsDropdown.SelectedIndex = -1;
                foreach(CheckBox hero in HeroesDropdown.Items)
                {
                    hero.IsChecked = false;
                }
                HeroesDropdown.SelectedIndex = 0;
                //Group.Text = "Group size";
                //Map.Text = "Map";                                         //these are commented out cuz they're antiquated now
                //Heroes.Text = "Heroes played";
                                
                
                //readding the handlers that were removed
                SR.GotFocus += SR_GotFocus;
                //Group.GotFocus += Group_GotFocus;
                //Map.GotFocus += Map_GotFocus;                             //again, obsolete and now removed
                //Heroes.GotFocus += Heroes_GotFocus;
                //DataError.Visibility = Visibility.Hidden;
            }
            catch
            {
                bool caught = false;
                try
                {
                    Int32.Parse(SR.Text);
                }
                catch
                {
                    caught = true;
                    SR.Text = "INVALID SR!";
                }
                if (!caught)
                {
                    //we need to insert some kind of error handling if no hero/map/group size is selected here so the user knows whats up. :^)
                }
                //DataError.Visibility = Visibility.Visible;
            }
        }

        private void AddMostRecentGameToScreen()
        {
            if (trackTheStats.isCalibrating)
            {
                if (Win.IsChecked == true)
                {
                    SR.Text = "Won";
                }
                else if (Loss.IsChecked == true)
                {
                    SR.Text = "Loss";
                }
                else
                {
                    SR.Text = "Tie";
                }
            }
            if (Game1.Text == "")
            {
                var heroes = CheckHeroesSelected();
                var breakupHeroes = heroes.Split('|');
                string printHeroes = "";
                foreach(string hero in breakupHeroes)
                {
                    if (breakupHeroes[breakupHeroes.Length - 1] != hero)
                        printHeroes = printHeroes + hero + " ";
                    else
                        printHeroes = printHeroes + hero;
                }
                ColorTheBackground(Game1);
                Game1.Text = SR.Text + ", " + GroupsDropdown.SelectedItem.ToString() + ", " + MapsDropdown.SelectedItem.ToString() + ", " + DetermineMapType(MapsDropdown.SelectedItem.ToString()) + ", " +
                    printHeroes + ", " + DateTime.Now;
                Game1.Visibility = Visibility.Visible;
            }
            else if(Game2.Text == "")
            {
                var heroes = CheckHeroesSelected();
                var breakupHeroes = heroes.Split('|');
                string printHeroes = "";
                foreach (string hero in breakupHeroes)
                {
                    if (breakupHeroes[breakupHeroes.Length - 1] != hero)
                        printHeroes = printHeroes + hero + " ";
                    else
                        printHeroes = printHeroes + hero;
                }
                ColorTheBackground(Game2);
                Game2.Text = SR.Text + ", " + GroupsDropdown.SelectedItem.ToString() + ", " + MapsDropdown.SelectedItem.ToString() + ", " + DetermineMapType(MapsDropdown.SelectedItem.ToString()) + ", " +
                    printHeroes + ", " + DateTime.Now;
                Game2.Visibility = Visibility.Visible;
            }
            else if (Game3.Text == "")
            {
                var heroes = CheckHeroesSelected();
                var breakupHeroes = heroes.Split('|');
                string printHeroes = "";
                foreach (string hero in breakupHeroes)
                {
                    if (breakupHeroes[breakupHeroes.Length - 1] != hero)
                        printHeroes = printHeroes + hero + " ";
                    else
                        printHeroes = printHeroes + hero;
                }
                ColorTheBackground(Game3);
                Game3.Text = SR.Text + ", " + GroupsDropdown.SelectedItem.ToString() + ", " + MapsDropdown.SelectedItem.ToString() + ", " + DetermineMapType(MapsDropdown.SelectedItem.ToString()) + ", " +
                    printHeroes + ", " + DateTime.Now;
                Game3.Visibility = Visibility.Visible;
            }
            else if (Game4.Text == "")
            {
                var heroes = CheckHeroesSelected();
                var breakupHeroes = heroes.Split('|');
                string printHeroes = "";
                foreach (string hero in breakupHeroes)
                {
                    if (breakupHeroes[breakupHeroes.Length - 1] != hero)
                        printHeroes = printHeroes + hero + " ";
                    else
                        printHeroes = printHeroes + hero;
                }
                ColorTheBackground(Game4);
                Game4.Text = SR.Text + ", " + GroupsDropdown.SelectedItem.ToString() + ", " + MapsDropdown.SelectedItem.ToString() + ", " + DetermineMapType(MapsDropdown.SelectedItem.ToString()) + ", " +
                    printHeroes + ", " + DateTime.Now;
                Game4.Visibility = Visibility.Visible;
            }
            else if (Game5.Text == "")
            {
                var heroes = CheckHeroesSelected();
                var breakupHeroes = heroes.Split('|');
                string printHeroes = "";
                foreach (string hero in breakupHeroes)
                {
                    if (breakupHeroes[breakupHeroes.Length - 1] != hero)
                        printHeroes = printHeroes + hero + " ";
                    else
                        printHeroes = printHeroes + hero;
                }
                ColorTheBackground(Game5);
                Game5.Text = SR.Text + ", " + GroupsDropdown.SelectedItem.ToString() + ", " + MapsDropdown.SelectedItem.ToString() + ", " + DetermineMapType(MapsDropdown.SelectedItem.ToString()) + ", " +
                    printHeroes + ", " + DateTime.Now;
                Game5.Visibility = Visibility.Visible;
            }
            else if (Game6.Text == "")
            {
                var heroes = CheckHeroesSelected();
                var breakupHeroes = heroes.Split('|');
                string printHeroes = "";
                foreach (string hero in breakupHeroes)
                {
                    if (breakupHeroes[breakupHeroes.Length - 1] != hero)
                        printHeroes = printHeroes + hero + " ";
                    else
                        printHeroes = printHeroes + hero;
                }
                ColorTheBackground(Game6);
                Game6.Text = SR.Text + ", " + GroupsDropdown.SelectedItem.ToString() + ", " + MapsDropdown.SelectedItem.ToString() + ", " + DetermineMapType(MapsDropdown.SelectedItem.ToString()) + ", " +
                    printHeroes + ", " + DateTime.Now;
                Game6.Visibility = Visibility.Visible;
            }
            else if (Game7.Text == "")
            {
                var heroes = CheckHeroesSelected();
                var breakupHeroes = heroes.Split('|');
                string printHeroes = "";
                foreach (string hero in breakupHeroes)
                {
                    if (breakupHeroes[breakupHeroes.Length - 1] != hero)
                        printHeroes = printHeroes + hero + " ";
                    else
                        printHeroes = printHeroes + hero;
                }
                ColorTheBackground(Game7);
                Game7.Text = SR.Text + ", " + GroupsDropdown.SelectedItem.ToString() + ", " + MapsDropdown.SelectedItem.ToString() + ", " + DetermineMapType(MapsDropdown.SelectedItem.ToString()) + ", " +
                    printHeroes + ", " + DateTime.Now;
                Game7.Visibility = Visibility.Visible;
            }
            else if (Game8.Text == "")
            {
                var heroes = CheckHeroesSelected();
                var breakupHeroes = heroes.Split('|');
                string printHeroes = "";
                foreach (string hero in breakupHeroes)
                {
                    if (breakupHeroes[breakupHeroes.Length - 1] != hero)
                        printHeroes = printHeroes + hero + " ";
                    else
                        printHeroes = printHeroes + hero;
                }
                ColorTheBackground(Game8);
                Game8.Text = SR.Text + ", " + GroupsDropdown.SelectedItem.ToString() + ", " + MapsDropdown.SelectedItem.ToString() + ", " + DetermineMapType(MapsDropdown.SelectedItem.ToString()) + ", " +
                    printHeroes + ", " + DateTime.Now;
                Game8.Visibility = Visibility.Visible;
            }
            else if (Game9.Text == "")
            {
                var heroes = CheckHeroesSelected();
                var breakupHeroes = heroes.Split('|');
                string printHeroes = "";
                foreach (string hero in breakupHeroes)
                {
                    if (breakupHeroes[breakupHeroes.Length - 1] != hero)
                        printHeroes = printHeroes + hero + " ";
                    else
                        printHeroes = printHeroes + hero;
                }
                ColorTheBackground(Game9);
                Game9.Text = SR.Text + ", " + GroupsDropdown.SelectedItem.ToString() + ", " + MapsDropdown.SelectedItem.ToString() + ", " + DetermineMapType(MapsDropdown.SelectedItem.ToString()) + ", " +
                    printHeroes + ", " + DateTime.Now;
                Game9.Visibility = Visibility.Visible;
            }
            else if (Game10.Text == "")
            {
                var heroes = CheckHeroesSelected();
                var breakupHeroes = heroes.Split('|');
                string printHeroes = "";
                foreach (string hero in breakupHeroes)
                {
                    if (breakupHeroes[breakupHeroes.Length - 1] != hero)
                        printHeroes = printHeroes + hero + " ";
                    else
                        printHeroes = printHeroes + hero;
                }
                ColorTheBackground(Game10);
                Game10.Text = SR.Text + ", " + GroupsDropdown.SelectedItem.ToString() + ", " + MapsDropdown.SelectedItem.ToString() + ", " + DetermineMapType(MapsDropdown.SelectedItem.ToString()) + ", " +
                    printHeroes + ", " + DateTime.Now;
                Game10.Visibility = Visibility.Visible;
            }
            else
            {
                Game1.Text = Game2.Text;
                Game1.Background = Game2.Background;
                Game2.Text = Game3.Text;
                Game2.Background = Game3.Background;
                Game3.Text = Game4.Text;
                Game3.Background = Game4.Background;
                Game4.Text = Game5.Text;
                Game4.Background = Game5.Background;
                Game5.Text = Game6.Text;
                Game5.Background = Game6.Background;
                Game6.Text = Game7.Text;
                Game7.Background = Game7.Background;
                Game7.Text = Game8.Text;
                Game7.Background = Game8.Background;
                Game8.Text = Game9.Text;
                Game8.Background = Game9.Background;
                Game9.Text = Game10.Text;
                Game9.Background = Game10.Background;
                var heroes = CheckHeroesSelected();
                var breakupHeroes = heroes.Split('|');
                string printHeroes = "";
                foreach (string hero in breakupHeroes)
                {
                    if (breakupHeroes[breakupHeroes.Length - 1] != hero)
                        printHeroes = printHeroes + hero + " ";
                    else
                        printHeroes = printHeroes + hero;
                }
                ColorTheBackground(Game10);
                Game10.Text = SR.Text + ", " + GroupsDropdown.SelectedItem.ToString() + ", " + MapsDropdown.SelectedItem.ToString() + ", " + DetermineMapType(MapsDropdown.SelectedItem.ToString()) + ", " +
                    printHeroes + ", " + DateTime.Now;
            }
        }

        private void SR_GotFocus(object sender, RoutedEventArgs e)
        {
            SR.Text = string.Empty;
            SR.GotFocus -= SR_GotFocus; //removing handler to prevent accidentally erasing data
        }

        /* private void Group_GotFocus(object sender, RoutedEventArgs e)
         {
             Group.Text = string.Empty;
             Group.GotFocus -= Group_GotFocus; //removing handler to prevent accidentally erasing data
         }

         private void Map_GotFocus(object sender, RoutedEventArgs e)
         {
             Map.Text = string.Empty;
             Map.GotFocus -= Map_GotFocus; //removing handler to prevent accidentally erasing data
         }

         private void Heroes_GotFocus(object sender, RoutedEventArgs e)
         {
             Heroes.Text = string.Empty;
             Heroes.GotFocus -= Heroes_GotFocus; //removing handler to prevent accidentally erasing data
         } */

        //antiquated :^)

        private void Window_Deactivated(object sender, EventArgs e)
        {
            this.Topmost = true;
            this.Activate();
        }

        private void AnalyticsLauncher_Click(object sender, RoutedEventArgs e)
        {
            var Statistics = new AnalysisWindow();
            Statistics.Show();
        }

        private void DetermineCalibratedSR()
        {
            CalibratedButton.Visibility = Visibility.Visible;
            CalibratedPrompt.Visibility = Visibility.Visible;
            CalibratedSRBox.Visibility = Visibility.Visible;
            trackTheStats.isCalibrating = false;
        }

        private void CalibrateSR_Click(object sender, RoutedEventArgs e)
        {
            trackTheStats.existingData.setCalibratedSR(Int32.Parse(CalibratedSRBox.Text.ToString()));
            trackTheStats.SaveData();
            CalibratedButton.Visibility = Visibility.Collapsed;
            CalibratedPrompt.Visibility = Visibility.Collapsed;
            CalibratedSRBox.Visibility = Visibility.Collapsed;
        }
        
        private void ColorTheBackground(TextBlock gameText)
        {
            if (trackTheStats.isCalibrating != true)
            {
                if (trackTheStats.differenceSR > 0)
                {
                    gameText.Background = Brushes.Green;
                }
                else if (trackTheStats.differenceSR < 0)
                {
                    gameText.Background = Brushes.Red;
                }
            }
            else
            {
                if (Win.IsChecked == true)
                {
                    gameText.Background = Brushes.Green;
                }
                else if (Loss.IsChecked == true)
                {
                    gameText.Background = Brushes.Red;
                }
            }
        }

        private void PopulateHeroes()
        {
            for (int i=0; i<heroAllowedNames.Length; i++)
            {
                HeroesDropdown.Items.Add(new CheckBox { Name = heroAllowedNames[i], Content = heroOptions[i] });
            }
        }

        private string CheckHeroesSelected()
        {
            string heroesChecked = "null";
            foreach(CheckBox hero in HeroesDropdown.Items)
            {
                if (hero.IsChecked==true)
                {
                    if (heroesChecked == "null")
                    {
                        heroesChecked = hero.Content.ToString();
                    }
                    else
                    {
                        heroesChecked = heroesChecked + "|" + hero.Content.ToString();
                    }
                }
            }
            return heroesChecked;
        }
    }
}
